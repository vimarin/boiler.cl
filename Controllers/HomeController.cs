﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Boiler.cl.Models;
using System.Text;
using System.Threading;
using System.Net.Mail;
using System.Net;

namespace Boiler.cl.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            Debug.WriteLine("Inicio Cargado");
            return View();
        }

        [HttpPost]
        public IActionResult SendContact(ContactModel e)
        {
            Debug.WriteLine("Post para Index - Se hizo <<Submit>> en Boton de Formulario");
            if (ModelState.IsValid)
            {
                Debug.WriteLine("Preparando email para contacto...");
                var subject = "Mensaje del Sitio Boiler.cl : " + e.nombre;
                var message = new StringBuilder();
                message.Append("Name: " + e.nombre + Environment.NewLine);
                message.Append("Email: " + e.email + Environment.NewLine);
                message.Append("Asunto: " + e.asunto + Environment.NewLine);
                message.Append("Mensaje : " + e.mensaje);

                Debug.WriteLine("Creando Thread para envío de mail...");
                var tEmail = new Thread(() => SendEmail(subject, message.ToString()));
                tEmail.Start();
                Debug.WriteLine("Correo Enviado : " + DateTime.Now.ToString());
            }
            return Content ("Mensaje enviado correctamente."); 

        }
        public void SendEmail(string subject, string message)
        {
            try
            {
                using (var mail = new MailMessage())
                {
                    const string email = "vimarin@outlook.cl";
                    const string password = "Chinitarika";

                    var loginInfo = new NetworkCredential(email, password);

                    mail.From = new MailAddress("vimarin@outlook.cl");
                    mail.To.Add(new MailAddress("contacto@boiler.cl"));
                    mail.Subject = subject;
                    mail.Body = message;
                    mail.IsBodyHtml = true;

                    try
                    {
                        using (var smtpClient = new SmtpClient("smtp-mail.outlook.com", 587))
                        {
                            smtpClient.EnableSsl = true;
                            smtpClient.UseDefaultCredentials = false;
                            smtpClient.Credentials = loginInfo;
                            smtpClient.Send(mail);
                        }
                    }

                    finally
                    {
                        //dispose the client
                        mail.Dispose();
                    }

                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                foreach (SmtpFailedRecipientException t in ex.InnerExceptions)
                {
                    var status = t.StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy || status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Debug.WriteLine("Error en envío de correo - retrying in 5 seconds.");
                        System.Threading.Thread.Sleep(5000);
                        // resend
                        // smtpClient.Send(mail);
                    }
                    else
                    {
                        Debug.WriteLine("Error al enviar el mensaje a {0}", t.FailedRecipient);
                    }
                }
            }
            catch (SmtpException Se)
            {
                // handle exception here
                Debug.WriteLine("Problemas en sesión SMTP : " + Se.ToString());
            }

            catch (Exception ex)
            {
                Debug.WriteLine("Problemas en el bucle: " + ex.ToString());
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
