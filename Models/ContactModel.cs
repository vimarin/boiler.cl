﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Boiler.cl.Models
{
    public class ContactModel
    {
        [Required]
        [Display(Name = "Nombre")]
        public string nombre { get; set; }
        
        [Required] [DataType(DataType.EmailAddress)]
        [Display(Name = "Correo Electrónico")]
        public string email { get; set; }
        
        [Required]
        [Display(Name = "Asunto")]
        public string asunto { get; set; }
        
        [Required]
        [Display(Name = "Mensaje")]
        public string mensaje { get; set; }
    }
}
